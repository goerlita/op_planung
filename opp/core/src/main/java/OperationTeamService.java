import java.util.Optional;

public interface OperationTeamService {

    /**
     * Processes the possible commands for an operation team
     * @param cmd the command to process
     * @return the processed operation team
     */
    OperationTeam process(OperationTeam.Command cmd);

    /**
     * Gets the operation  team by id
     * @param id the id of the operation team
     * @return the operation team
     */
    Optional<OperationTeam> getOperationTeam(Id<OperationTeam> id);
}
