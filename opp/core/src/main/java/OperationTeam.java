import java.time.Instant;
import java.util.List;

/**
 * OperationTeam class with its attributes, filters and commands.
 * @param id unique id of the operation team
 * @param opStaffs the list of all op staffs in this team
 * @param teamName the name of the team
 * @param lastUpdate the last time this object was changed
 */
public record OperationTeam(
        Id<OperationTeam> id,
        List<OPStaff> opStaffs,
        String teamName,
        Instant lastUpdate
) {

    /**
     * Operation commands
     */
    public sealed interface Command permits
            Create,
            Delete,
            AssignStaff,
            RemoveStaff
    {}

    /**
     * Create command to create a new operation team
     * @param teamName the name of the new operation team
     * @param opStaffs the list of all op staffs in the new operation team
     */
    public record Create(
            String teamName,
            List<OPStaff> opStaffs
    ) implements Command {}

    /**
     * Command to delete an existing operation team
     * @param id the id of the already existing operation team
     */
    public record Delete(
            Id<OperationTeam> id
    ) implements Command {}

    /**
     * Command to assign an op staff to an operation team
     * @param operationTeamId the id of the already existing operation team
     * @param opStaffId the id of the already existing op staff
     */
    public record AssignStaff(
            Id<OperationTeam> operationTeamId,
            Id<OPStaff> opStaffId
    ) implements Command {}

    /**
     * Command to remove an op staff from an operation team
     * @param operationTeamId the id of the already existing operation team
     * @param opStaffId the id of the assigned op staff
     */
    public record RemoveStaff(
            Id<OperationTeam> operationTeamId,
            Id<OPStaff> opStaffId
    ) implements Command {}
}
