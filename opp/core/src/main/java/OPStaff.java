import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

/**
 * OPStaff class with its attributes, filters and commands.
 * @param id unique id of the op staff
 * @param role the role of the op staff
 * @param specialty the specialty of the op staff
 * @param lastUpdate the last time this object was changed
 */
public record OPStaff(
        Id<OPStaff> id,
        Role role,
        Specialty specialty,
        Instant lastUpdate
) {

    /**
     * OPStaff commands
     */
    public sealed interface Command permits Create, Delete, Update { }

    /**
     * Create command to create a new op staff
     * @param role the role of the op staff
     * @param specialty the specialty of the op staff
     */
    public record Create(
            Role role,
            Specialty specialty
    ) implements Command {}

    /**
     * Update command to update an existing op staff
     * @param id the id of the existing op staff
     * @param role optional new role
     * @param specialty optional new specialty
     */
    public record Update(
            Id<OPStaff> id,
            Optional<Role> role,
            Optional<Specialty> specialty
    ) implements Command {}

    /**
     * Delete command to delete an existing op staff
     * @param id the id of the existing op staff
     */
    public record Delete(
            Id<OPStaff> id
    ) implements Command {}
}
