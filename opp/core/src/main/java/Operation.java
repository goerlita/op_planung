import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

/**
 * Operation class with its attributes, filters and commands.
 * @param id unique id of the operation
 * @param date the planned date of the operation
 * @param startTime the planned starting time of the operation
 * @param endTime the planned ending time of the operation
 * @param patientId the id of the patient
 * @param operationTeamId the id of the operationTeam
 * @param roomId the id of the room
 * @param lastUpdate the last time this object was changed
 */
public record Operation(
        Id<Operation> id,
        LocalDate date,
        LocalTime startTime,
        LocalTime endTime,
        Id<Patient> patientId,
        Id<OperationTeam> operationTeamId,
        Id<Room> roomId,
        Instant lastUpdate
) {

    /**
     * Operation filter
     * @param date filter with date
     */
    public record Filter(
        Optional<LocalDate> date
    ) {
        public static final Filter NONE =
            new Filter(
                Optional.empty()
            );
    }

    /**
     * Operation commands
     */
    public sealed interface Command permits Create, Update, Delete { }

    /**
     * Create command to create a new Operation
     * @param date the date of the new Operation
     * @param startTime the starting time of the new Operation
     * @param endTime the ending time of the new Operation
     * @param patientId the id of the patient
     * @param operationTeamId the id of the operationTeam
     * @param roomId the id of the room
     */
    public record Create(
            LocalDate date,
            LocalTime startTime,
            LocalTime endTime,
            Id<Patient> patientId,
            Id<OperationTeam> operationTeamId,
            Id<Room> roomId
    ) implements Command {}

    /**
     * Update command to update an already existing operation.
     * @param id the id of the already existing operation
     * @param date optional new date
     * @param startTime optional new start time
     * @param endTime optional new end time
     * @param patientId optional new patient id
     * @param operationTeamId optional new operation team
     * @param roomId optional new room
     */
    public record Update(
            Id<Operation> id,
            Optional<LocalDate> date,
            Optional<LocalTime> startTime,
            Optional<LocalTime> endTime,
            Optional<Id<Patient>> patientId,
            Optional<Id<OperationTeam>> operationTeamId,
            Optional<Id<Room>> roomId
    ) implements Command {}

    /**
     * Command to delete an existing operation
     * @param id the id of the already existing operation
     */
    public record Delete(
            Id<Operation> id
    ) implements Command {}

}
