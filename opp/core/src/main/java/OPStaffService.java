import java.util.List;
import java.util.Optional;

/**
 * Available Services for OPStaff
 */
public interface OPStaffService {

    /**
     * Processes the possible commands for an OPStaff
     * @param cmd the command to process
     * @return the processed OPStaff
     */
    OPStaff process(OPStaff.Command cmd);

    /**
     * Gets the Operation by ID
     * @param id the ID of the OPStaff
     * @return the OPStaff
     */
    Optional<OPStaff> getOPStaff(Id<OPStaff> id);
}