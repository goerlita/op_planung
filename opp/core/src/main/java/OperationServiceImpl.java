import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * The implemented Services of the Operation
 */
public class OperationServiceImpl implements OperationService {

    private final Repository repo;

    /**
     * Initializes the repository to operate the services
     * @param repo the repository to work with
     */
    public OperationServiceImpl(Repository repo) {
        this.repo = repo;
    }

    @Override
    public Operation process(Operation.Command cmd) {
        return switch (cmd){
            case Operation.Create cr -> create(cr);
            case Operation.Update up -> update(up);
            case Operation.Delete del -> delete(del);
        };
    }

    @Override
    public Optional<Operation> getOperation(Id<Operation> id) {
        return repo.findOperation(id);
    }

    @Override
    public List<Operation> getOperations(Operation.Filter filter) {
        return repo.findOperations(filter);
    }

    /**
     * Creates a new operation with the set parameters
     * @param cr the create command that includes the parameters to create the operation with
     * @return the created operation
     */
    public Operation create(Operation.Create cr) {
        Operation operation =
                new Operation(
                        repo.operationId(),
                        cr.date(),
                        cr.startTime(),
                        cr.endTime(),
                        cr.patientId(),
                        cr.operationTeamId(),
                        cr.roomId(),
                        Instant.now());

        repo.save(operation);

        return operation;
    }

    /**
     * Updates an existing operation with the set parameters
     * @param up the update command that includes the parameters to update the operation with
     * @return the updated operation
     */
    public Operation update(Operation.Update up) {

        Optional<Operation> foundOperation =  repo.findOperation(up.id());

        Operation operation = null;
        if (foundOperation.isPresent()) {
            Operation currentOperation = foundOperation.get();

            operation =
                    new Operation(
                            currentOperation.id(),
                            up.date().orElse(currentOperation.date()),
                            up.startTime().orElse(currentOperation.startTime()),
                            up.endTime().orElse(currentOperation.endTime()),
                            up.patientId().orElse(currentOperation.patientId()),
                            up.operationTeamId().orElse(currentOperation.operationTeamId()),
                            up.roomId().orElse(currentOperation.roomId()),
                            Instant.now());

            repo.save(operation);
        }

        return operation;
    }

    /**
     * Deletes an existing operation with the given id
     * @param del the delete command that includes the id to delete the operation with
     * @return the deleted operation
     */
    public Operation delete(Operation.Delete del) {

        Optional<Operation> foundOperation =  repo.findOperation(del.id());
        if (foundOperation.isPresent()) {
            List<PreparationNote> allPreparationNotes = repo.findAllPreparationNotes();
            for (PreparationNote preparationNote : allPreparationNotes) {
                if (preparationNote.operationId().value().equals(foundOperation.get().id().value())) {
                    repo.deletePreparationNote(preparationNote.id());
                }
            }
            return repo.deleteOperation(del.id());
        }
        return null;
    }
}
