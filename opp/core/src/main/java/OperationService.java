import java.util.List;
import java.util.Optional;

/**
 * Available Services for operations
 */
public interface OperationService {

    /**
     * Processes the possible commands for an operation
     * @param cmd the command to process
     * @return the processed operation
     */
    Operation process(Operation.Command cmd);

    /**
     * Gets the operation by id
     * @param id the id of the operation
     * @return the operation
     */
    Optional<Operation> getOperation(Id<Operation> id);

    /**
     * Gets a list of operations that are matching the filter
     * @param filter the filter to match
     * @return the matching operations
     */
    List<Operation> getOperations(Operation.Filter filter);
}
