import java.time.Instant;

/**
 * Room class with its attributes, filters and commands.
 * @param id unique id of the room
 * @param roomName the name of the room
 * @param lastUpdate the last time this object was changed
 */
public record Room (
    Id<Room> id,
    String roomName,
    Instant lastUpdate
) {

    /**
     * Room commands
     */
    public sealed interface Command permits
            Create,
            Update,
            Delete
    {}

    /**
     * Command to create a new room
     * @param roomName the name of the new room
     */
    public record Create(
            String roomName
    ) implements Command {}

    /**
     * Command to update an existing room
     * @param id the id of the existing room
     * @param roomName the new room name
     */
    public record Update(
            Id<Room> id,
            String roomName
    ) implements Command{}

    /**
     * Command to delete an existing room
     * @param id the id of the room to delete
     */
    public record Delete(
            Id<Room> id
    ) implements Command{}

}
