/**
 * An Enum containing all possible specialities for an OPStaff
 */
public enum Specialty {
    ORTHOPEDICS,
    CARDIOLOGY,
    UROLOGY,
    GASTROENTEROLOGY
}
