import java.time.Instant;
import java.util.Optional;

/**
 * The implemented Services of the OperationTeam
 */
public class OperationTeamServiceImpl implements OperationTeamService{

    private final Repository repo;

    /**
     * Initializes the repository to operate the services
     * @param repo the repository to work with
     */
    public OperationTeamServiceImpl(Repository repo) {
        this.repo = repo;
    }

    @Override
    public OperationTeam process(OperationTeam.Command cmd) {
        return switch(cmd){
            case OperationTeam.Create cr -> create(cr);
            case OperationTeam.Delete del -> delete(del);
            case OperationTeam.AssignStaff as -> assignStaff(as);
            case OperationTeam.RemoveStaff rs -> removeStaff(rs);
        };
    }

    @Override
    public Optional<OperationTeam> getOperationTeam(Id<OperationTeam> operationTeamId) {
        return repo.findOperationTeam(operationTeamId);
    }

    /**
     * Creates a new operation team with the set parameters.
     * Assigns the op staffs in the parameters to the operation team
     * @param cr the create command that includes the parameters to create the operation team with
     * @return the created operation team
     */
    private OperationTeam create(OperationTeam.Create cr) {

        Id<OperationTeam> generatedOperationTeamId = repo.operationTeamId();
        OperationTeam operationTeam = new OperationTeam(
                generatedOperationTeamId,
                cr.opStaffs(),
                cr.teamName(),
                Instant.now()
        );

        repo.save(operationTeam);

        for(OPStaff opstaff : cr.opStaffs()) {
            repo.assignOPStaffToOperationTeam(generatedOperationTeamId, opstaff.id());
        }

        return operationTeam;
    }

    /**
     * Deletes an existing operation team with the given id.
     * Removes the op staffs from the operation team
     * @param del the delete command that includes the id to delete the operation team with
     * @return the deleted operation team
     */
    private OperationTeam delete(OperationTeam.Delete del) {

        Optional<OperationTeam> foundOperationTeam = repo.findOperationTeam(del.id());

        if(foundOperationTeam.isPresent()) {

            if (!foundOperationTeam.get().opStaffs().isEmpty()) {
                for (OPStaff opStaff : foundOperationTeam.get().opStaffs()) {
                    repo.removeOPStaffInOperationTeam(del.id(), opStaff.id());
                }
                return repo.deleteOperationTeam(del.id());
            }
            return foundOperationTeam.get();
        }
        return null;
    }

    /**
     * Assigns the op staff to the operation team
     * @param as the assignStaff command that includes the ids of the op staff and operation team
     * @return the operation team with the assigned staff
     */
    private OperationTeam assignStaff(OperationTeam.AssignStaff as) {

        OPStaff opStaffToAssign = repo.findOPStaff(as.opStaffId()).get();

        OperationTeam operationTeamToAssignStaff = repo.findOperationTeam(as.operationTeamId()).get();

        if(repo.assignOPStaffToOperationTeam(as.operationTeamId(), as.opStaffId())) {
            operationTeamToAssignStaff.opStaffs().add(opStaffToAssign);
            return operationTeamToAssignStaff;
        } else {
            return null;
        }
    }

    /**
     * Removes the op staff from the operation team
     * @param rs the removeStaff command that includes the ids of the op staff and operation team
     * @return the operation team with the removed staff
     */
    private OperationTeam removeStaff(OperationTeam.RemoveStaff rs) {

        Optional<OperationTeam> findOperationTeam = repo.findOperationTeam(rs.operationTeamId());

        if(findOperationTeam.isPresent()){
            if(repo.removeOPStaffInOperationTeam(rs.operationTeamId(), rs.opStaffId())){
                Optional<OperationTeam> newOperationTeam = repo.findOperationTeam(rs.operationTeamId());
                return newOperationTeam.get();
            }
        }
        return null;
    }
}
