import java.time.Instant;
import java.util.Optional;

public class PreparationNoteServiceImpl implements PreparationNoteService {

    private final Repository repo;

    /**
     * Initializes the repository to operate the services
     * @param repo the repository to work with
     */
    public PreparationNoteServiceImpl(Repository repo) {
        this.repo = repo;
    }

    @Override
    public PreparationNote process(PreparationNote.Command cmd) {
        return switch (cmd){
            case PreparationNote.Create cr -> create(cr);
            case PreparationNote.Update up -> update(up);
            case PreparationNote.Delete del -> delete(del);
        };
    }

    @Override
    public Optional<PreparationNote> getPreparationNote(Id<PreparationNote> id){
        return repo.findPreparationNote(id);
    }

    /**
     * Creates a new preparation note with the set parameters
     * @param cr the create command that includes the parameters to create the preparation note with
     * @return the created preparation note
     */
    public PreparationNote create(PreparationNote.Create cr) {
        PreparationNote preparationNote =
                new PreparationNote(
                        repo.preparationNoteId(),
                        cr.note(),
                        cr.operationId(),
                        Instant.now());

        repo.save(preparationNote);

        return preparationNote;
    }

    /**
     * Updates an existing preparation note with the set parameters
     * @param up the update command that includes the parameters to update the preparation note with
     * @return the updated preparation note
     */
    public PreparationNote update(PreparationNote.Update up) {

        Optional<PreparationNote> foundPreparationNote =  repo.findPreparationNote(up.id());

        PreparationNote preparationNote = null;
        if (foundPreparationNote.isPresent()) {
            PreparationNote currentPreparationNote = foundPreparationNote.get();

            preparationNote =
                    new PreparationNote(
                            currentPreparationNote.id(),
                            up.note(),
                            currentPreparationNote.operationId(),
                            Instant.now());

            repo.save(preparationNote);
        }

        return preparationNote;
    }

    /**
     * Deletes an existing preparation note with the given id
     * @param del the delete command that includes the id to delete the preparation note with
     * @return the deleted preparation note
     */
    private PreparationNote delete(PreparationNote.Delete del) {

        Optional<PreparationNote> returnedPreparationNote = repo.findPreparationNote(del.id());

        if(returnedPreparationNote.isPresent()) {
             return repo.deletePreparationNote(del.id());
        }
        return null;
    }
}
