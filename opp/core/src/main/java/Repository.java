import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public interface Repository {

    /**
     * Return an unused operation id
     * @return generated operation id
     */
    Id<Operation> operationId();

    /**
     * Saves a new operation into the database, updates an already existing entry otherwise
     * @param operation the operation to save/update to the database
     */
    void save(Operation operation);

    /**
     * Returns the operation with the given id.
     * @param id the id to search for
     * @return the found operation, if it exists
     */
    Optional<Operation> findOperation(Id<Operation> id);

    /**
     * Returns a list of all operation entries matching the filter.
     * @param filter the filter
     * @return the matching entries
     */
    List<Operation> findOperations(Operation.Filter filter);

    /**
     * Deletes the operation with the matching id.
     * @param id the id of the operation to delete
     * @return the deleted operation
     */
    Operation deleteOperation(Id<Operation> id);


    /**
     * Return an unused op staff id
     * @return generated op staff id
     */
    Id<OPStaff> opStaffId();

    /**
     * Saves a new op staff into the database, updates an already existing entry otherwise
     * @param opStaff the op staff to save/update to the database
     */
    void save(OPStaff opStaff);

    /**
     * Returns the op staff with the given id.
     * @param id the id to search for
     * @return the found op staff, if it exists
     */
    Optional<OPStaff> findOPStaff(Id<OPStaff> id);

    /**
     * Returns a list of all op staffs
     * @return all op staff entries
     */
    List<OPStaff> findAllOPStaffs();

    /**
     * Deletes the op staff with the matching id.
     * @param id the id of the op staff to delete
     * @return the deleted op staff
     */
    OPStaff deleteOPStaff(Id<OPStaff> id);



    /**
     * Return an unused operation team id
     * @return generated operation team id
     */
    Id<OperationTeam> operationTeamId();

    /**
     * Saves a new operation team into the database, updates an already existing entry otherwise
     * @param operationTeam the operation team to save/update to the database
     */
    void save(OperationTeam operationTeam);

    /**
     * Returns the operation team with the given id.
     * @param id the id to search for
     * @return the found operation team, if it exists
     */
    Optional<OperationTeam> findOperationTeam(Id<OperationTeam> id);

    /**
     * Returns a list of all operation teams
     * @return all operation team entries
     */
    //List<OperationTeam> findOperationTeams() throws SQLException;

    /**
     * Deletes the operation team with the matching id.
     * @param id the id of the operation team to delete
     * @return the deleted operation team
     */
    OperationTeam deleteOperationTeam(Id<OperationTeam> id);

    /**
     * Return an unused preparation note id
     * @return generated preparation note id
     */
    Id<PreparationNote> preparationNoteId();

    /**
     * Saves a new preparation note into the database, updates an already existing entry otherwise
     * @param preparationNote the preparation note to save/update to the database
     */
    void save(PreparationNote preparationNote);

    /**
     * Returns the preparation note with the given id.
     * @param id the id to search for
     * @return the found preparation note, if it exists
     */
    Optional<PreparationNote> findPreparationNote(Id<PreparationNote> id);

    /**
     * Returns a list of all preparation notes
     * @return all preparation note entries
     */
    List<PreparationNote> findAllPreparationNotes();

    /**
     * Deletes the preparation note with the matching id.
     * @param id the id of the preparation note to delete
     * @return the deleted preparation note
     */
    PreparationNote deletePreparationNote(Id<PreparationNote> id);


    /**
     * Return an unused room id
     * @return generated room id
     */
    Id<Room> roomId();

    /**
     * Saves a new room into the database, updates an already existing entry otherwise
     * @param room the operation to save/update to the database
     */
    void save(Room room);

    /**
     * Returns the preparation note with the given id.
     * @param id the id to search for
     * @return the found preparation note, if it exists
     */
    Optional<Room> findRoom(Id<Room> id);

    /**
     * Deletes the room with the matching id.
     * @param id the id of the room to delete
     * @return the deleted room
     */
    Room deleteRoom(Id<Room> id);


    /**
     * Assigns an op staff to an operation team
     * @param operationTeamId the id of the operation team to assign the op staff to
     * @param opStaffId the id of the op staff
     * @return true if the assign was successful, otherwise false
     */
    Boolean assignOPStaffToOperationTeam(Id<OperationTeam> operationTeamId, Id<OPStaff> opStaffId);

    /**
     * Removes an op staff from an operation team
     * @param operationTeamId the id of the operation team to remove the op staff from
     * @param opStaffId the id of the op staff
     * @return true if the remove was successful, otherwise false
     */
    Boolean removeOPStaffInOperationTeam(Id<OperationTeam> operationTeamId, Id<OPStaff> opStaffId);

    /**
     * Searches if the op staff is already assigned to the operation team
     * @param operationTeamId the id of the operation team
     * @param opStaffId the id of the op staff
     * @return true if the op staff is already assigned, otherwise false
     */
    Boolean findOperationTeamOPStaff(Id<OperationTeam> operationTeamId, Id<OPStaff> opStaffId);

    Boolean findOPStaffOperationTeams(Id<OPStaff> opStaffId);

}
