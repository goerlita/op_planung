import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;

/**
 * PreparationNote class with its attributes, filters and commands.
 * @param id unique id of the preparation note
 * @param note the text of the preparation note
 * @param operationId the id of the operation it is assigned to
 * @param lastUpdate the last time this object was changed
 */
public record PreparationNote (

    Id<PreparationNote> id,
    String note,
    Id<Operation> operationId,
    Instant lastUpdate
) {

    /**
     * PreparationNote commands
     */
    public sealed interface Command permits Create, Update, Delete { }

    /**
     * Create command to create a new preparation note
     * @param note the text
     * @param operationId the operation id
     */
    public record Create(
            String note,
            Id<Operation> operationId
    ) implements Command {}

    /**
     * Update command to update an existing preparation note
     * @param id the id of the existing preparation note
     * @param note the new text
     */
    public record Update(
            Id<PreparationNote> id,
            String note
    ) implements Command {}

    /**
     * Delete command to delete an existing preparation note
     * @param id the id of the existing preparation note
     */
    public record Delete(
            Id<PreparationNote> id
    ) implements Command {}


}
