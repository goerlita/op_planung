import java.util.Optional;

public interface PreparationNoteService {

    /**
     * Processes the possible commands for a preparation note
     * @param cmd the command to process
     * @return the processed preparation note
     */
    PreparationNote process(PreparationNote.Command cmd);

    /**
     * Gets the preparation note by ID
     * @param id the ID of the preparation note
     * @return the preparation note
     */
    Optional<PreparationNote> getPreparationNote(Id<PreparationNote> id);
}
