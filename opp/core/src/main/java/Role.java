/**
 * An Enum that contains all possible roles for an OPStaff
 */
public enum Role {
    SURGEON,
    ASSISTANT
}
