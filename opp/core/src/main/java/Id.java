/**
 * Used to give each individual class it's id
 * @param value the id
 * @param <T> the class
 */
public record Id<T>(String value)
{
    @Override
    public String toString(){
        return value;
    }
}
