import java.util.Optional;


public interface RoomService {

    /**
     * Processes the possible commands for a room
     * @param cmd the command to process
     * @return the processed room
     */
    Room process(Room.Command cmd) throws Exception;

    /**
     * Gets the room by ID
     * @param id the ID of the room
     * @return the room
     */
    Optional<Room> getRoom(Id<Room> id);
}
