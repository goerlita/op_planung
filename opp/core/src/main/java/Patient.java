/**
 * A class used to simulate an existing Patient.
 * @param id unique id of the patient
 */
public record Patient (
        Id<Patient> id
) {
}
