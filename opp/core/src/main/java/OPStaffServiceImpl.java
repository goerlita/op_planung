import java.time.Instant;
import java.util.Optional;
import java.util.List;

public class OPStaffServiceImpl implements OPStaffService{

    private final Repository repo;

    /**
     * Initializes the repository to operate the services
     * @param repo the repository to work with
     */
    public OPStaffServiceImpl(Repository repo) {
        this.repo = repo;
    }

    @Override
    public OPStaff process(OPStaff.Command cmd) {
        return switch (cmd){
            case OPStaff.Create cr -> create(cr);
            case OPStaff.Update up -> update(up);
            case OPStaff.Delete del -> delete(del);
        };
    }

    @Override
    public Optional<OPStaff> getOPStaff(Id<OPStaff> id) {
        return repo.findOPStaff(id);
    }

    /**
     * Creates a new op staff with the set parameters
     * @param cr the create command that includes the parameters to create the op staff with
     * @return the created op staff
     */
    public OPStaff create(OPStaff.Create cr) {
        OPStaff opStaff =
                new OPStaff(
                        repo.opStaffId(),
                        cr.role(),
                        cr.specialty(),
                        Instant.now());

        repo.save(opStaff);

        return opStaff;
    }

    /**
     * Updates an existing op staff with the set parameters
     * @param up the update command that includes the parameters to update the op staff with
     * @return the updated op staff
     */
    public OPStaff update(OPStaff.Update up) {

        Optional<OPStaff> foundOPStaff =  repo.findOPStaff(up.id());

        OPStaff opStaff = null;
        if (foundOPStaff.isPresent()) {
            OPStaff currentOPStaff = foundOPStaff.get();

            opStaff =
                    new OPStaff(
                            currentOPStaff.id(),
                            up.role().orElse(currentOPStaff.role()),
                            up.specialty().orElse((currentOPStaff.specialty())),
                            Instant.now()
                    );
            repo.save(opStaff);
        }

        return opStaff;
    }

    /**
     * Deletes an existing op staff with the given id
     * @param del the delete command that includes the id to delete the op staff with
     * @return the deleted op staff
     */
    public OPStaff delete(OPStaff.Delete del) {

        Optional<OPStaff> foundOPStaff = repo.findOPStaff(del.id());

        if (foundOPStaff.isPresent()) {
            if(!repo.findOPStaffOperationTeams(del.id())){
                return repo.deleteOPStaff(del.id());
            }
            return foundOPStaff.get();
        }

        return null;
    }
}
