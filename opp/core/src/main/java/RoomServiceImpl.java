import java.time.Instant;
import java.util.Optional;

public class RoomServiceImpl implements RoomService {

    private final Repository repo;

    public RoomServiceImpl(Repository repo) {
        this.repo = repo;
    }

    @Override
    public Room process(Room.Command cmd) throws Exception {
        return switch (cmd){
            case Room.Create cr -> create(cr);
            case Room.Update up -> update(up);
            case Room.Delete del -> delete(del);
        };
    }

    @Override
    public Optional<Room> getRoom(Id<Room> id){
        return repo.findRoom(id);
    }

    /**
     * Creates a new room with the set parameters
     * @param cr the create command that includes the parameters to create the room with
     * @return the created room
     */
    public Room create(Room.Create cr) throws Exception {
        Room room = new Room(
                repo.roomId(),
                cr.roomName(),
                Instant.now());

        repo.save(room);

        return room;
    }

    /**
     * Updates an existing room with the set parameters
     * @param up the update command that includes the parameters to update the room with
     * @return the updated room
     */
    public Room update(Room.Update up) throws Exception {

        Optional<Room> foundRoom = repo.findRoom(up.id());

        Room room = null;
        if (foundRoom.isPresent()) {
            Room currentRoom = foundRoom.get();

            room = new Room(
                    currentRoom.id(),
                    up.roomName(),
                    Instant.now());

            repo.save(room);
        }

        return room;
    }

    /**
     * Deletes an existing room with the given id
     * @param del the delete command that includes the id to delete the room with
     * @return the deleted room
     */
    private Room delete(Room.Delete del) throws Exception {

        Optional<Room> returnedRoom = repo.findRoom(del.id());

        if(returnedRoom.isPresent()) {
            return repo.deleteRoom(del.id());
        }
        return null;
    }
}

