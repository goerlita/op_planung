import org.junit.BeforeClass;
import org.junit.Test;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;


public final class Tests
{

  private static Repository repo = null;

  private static Patient testPatient = null;

  private static Operation testOperation = null;

  private static OperationService operationService;

  private static OPStaff testOPStaff1 = null;
  private static OPStaff testOPStaff2 = null;
  private static OPStaff testOPStaff3 = null;

  private static OPStaffService opStaffService;

  private static OperationTeam testOperationTeam1 = null;
  private static OperationTeam testOperationTeam2 = null;

  private static OperationTeamService operationTeamService;

  private static PreparationNote testPreparationNote1 = null;
  private static PreparationNote testPreparationNote2 = null;


  private static PreparationNoteService preparationNoteService;

  private static Room testRoom1 = null;
  private static Room testRoom2 = null;

  private static RoomService roomService;

  @BeforeClass
  public static void init() {

    System.setProperty("opp.repo.jdbc.url",      "jdbc:postgresql:postgres");
    System.setProperty("opp.repo.jdbc.user",     "postgres");
    System.setProperty("opp.repo.jdbc.password", "1234");

    repo = JDBCRepository.instance();

    testPatient = new Patient(
            new Id<>("patient1111")
    );

    testOPStaff1 = new OPStaff(
            new Id<>("opStaff1111"),
            Role.ASSISTANT,
            Specialty.UROLOGY,
            Instant.now()
    );

    testOPStaff2 = new OPStaff(
            new Id<>("opStaff2222"),
            Role.ASSISTANT,
            Specialty.CARDIOLOGY,
            Instant.now());

    testOPStaff3 = new OPStaff(
            new Id<>("opStaff3333"),
            Role.SURGEON,
            Specialty.GASTROENTEROLOGY,
            Instant.now());

    List<OPStaff> opStaffList1 = new ArrayList<>();
    opStaffList1.add(testOPStaff1);
    opStaffList1.add(testOPStaff2);

    testOperationTeam1 = new OperationTeam(
            new Id<>("operationTeam1111"),
            opStaffList1,
            ("Team1"),
            Instant.now()
    );

    List<OPStaff> opStaffList2 = new ArrayList<>();
    opStaffList2.add(testOPStaff3);

    testOperationTeam2 = new OperationTeam(
            new Id<>("operationTeam2222"),
            opStaffList2,
            ("Team2"),
            Instant.now()
    );

    testRoom1 = new Room(
            new Id<>("room1111"),
            ("Room1"),
            Instant.now()
    );

    testRoom2 = new Room(
            new Id<>("room2222"),
            ("Room2"),
            Instant.now()
    );

    testOperation = new Operation(
            new Id<>("operation1111"),
            LocalDate.of(2024, 5, 24),
            LocalTime.of(13, 35, 0),
            LocalTime.of(14, 0, 0),
            testPatient.id(),
            testOperationTeam1.id(),
            testRoom1.id(),
            Instant.now()
    );

    testPreparationNote1 = new PreparationNote(
            new Id<>("note1111"),
            ("note1"),
            testOperation.id(),
            Instant.now()
    );

    testPreparationNote2 = new PreparationNote(
            new Id<>("note2222"),
            ("note2"),
            testOperation.id(),
            Instant.now()
    );

    operationService = new OperationServiceImpl(repo);

    opStaffService = new OPStaffServiceImpl(repo);

    operationTeamService = new OperationTeamServiceImpl(repo);

    preparationNoteService = new PreparationNoteServiceImpl(repo);

    roomService = new RoomServiceImpl(repo);

  }

  @Test
  public void testCreateOperation() {

    repo.save(testRoom1);
    repo.save(testOperationTeam1);

    Operation.Create createCommand = new Operation.Create(
            LocalDate.of(2025, 5, 11),
            LocalTime.of(10, 30, 0),
            LocalTime.of(11, 0, 0),
            testPatient.id(),
            testOperationTeam1.id(),
            testRoom1.id()
    );

    Operation createdOperation = operationService.process(createCommand);

    assertTrue(
            repo.findOperation(createdOperation.id()).isPresent()
    );
  }

  @Test
  public void testUpdateOperation() throws Exception {

    repo.save(testRoom1);
    repo.save(testOperationTeam1);
    repo.save(testOperation);

    Operation.Update updateCommand = new Operation.Update(
            testOperation.id(),
            Optional.empty(),
            Optional.of(LocalTime.of(23,59,0)),
            Optional.empty(),
            Optional.empty(),
            Optional.empty(),
            Optional.empty()
    );

    Operation updateOperation = operationService.process(updateCommand);

    Optional<Operation> readOperation = repo.findOperation(updateOperation.id());

    if (readOperation.isPresent()) {
      assertNotEquals(readOperation.get().startTime(), testOperation.startTime());

      assertEquals(readOperation.get().date(), testOperation.date());

      assertEquals(readOperation.get().endTime(), testOperation.endTime());

      assertNotEquals(readOperation.get().lastUpdate(), testOperation.lastUpdate());
    } else {
      throw new Exception();
    }
  }

  @Test
  public void testDeleteOperation() {

    repo.save(testRoom1);
    repo.save(testOperationTeam1);
    repo.save(testOperation);
    repo.save(testPreparationNote1);
    repo.save(testPreparationNote2);

    Operation.Delete deleteCommand = new Operation.Delete(
            testOperation.id()
    );

    Operation deleteOperation = operationService.process(deleteCommand);

    assertFalse(repo.findOperation(deleteOperation.id()).isPresent());
  }

  @Test
  public void testGetOperation() throws Exception {

    repo.save(testRoom1);
    repo.save(testOperationTeam1);
    repo.save(testOperation);

    Optional<Operation> readOperation = operationService.getOperation(testOperation.id());

    if (readOperation.isPresent()) {
      assertEquals(testOperation.id(), readOperation.get().id());
      assertEquals(testOperation.date(), readOperation.get().date());
      assertEquals(testOperation.startTime(), readOperation.get().startTime());
      assertEquals(testOperation.endTime(), readOperation.get().endTime());
      assertEquals(testOperation.patientId(), readOperation.get().patientId());
      assertEquals(testOperation.operationTeamId(), readOperation.get().operationTeamId());
      assertEquals(testOperation.roomId(), readOperation.get().roomId());

    } else {
      throw new Exception();
    }
  }

  @Test
  public void testGetOperations() {

    repo.save(testRoom1);
    repo.save(testOperationTeam1);
    repo.save(testOperation);

    LocalDate testDate = LocalDate.of(2024, 5, 24);

    Operation.Filter filter = new Operation.Filter(Optional.of(testDate));

    List<Operation> operations = operationService.getOperations(filter);

    for (Operation operation : operations) {
      assertEquals(operation.date(), testDate);
    }
  }


  @Test
  public void testCreateOPStaff() {

    OPStaff.Create createCommand = new OPStaff.Create(
            Role.ASSISTANT,
            Specialty.GASTROENTEROLOGY
    );

    OPStaff createdOPStaff = opStaffService.process(createCommand);

    assertTrue(
            repo.findOPStaff(createdOPStaff.id()).isPresent()
    );
  }

  @Test
  public void testUpdateOPStaff() throws Exception {

    repo.save(testOPStaff1);

    OPStaff.Update updateCommand = new OPStaff.Update(
            testOPStaff1.id(),
            Optional.empty(),
            Optional.of(Specialty.CARDIOLOGY)
    );

    OPStaff updatedOperation = opStaffService.process(updateCommand);

    Optional<OPStaff> readOPStaff = repo.findOPStaff(updatedOperation.id());

    if (readOPStaff.isPresent()) {
      assertNotEquals(readOPStaff.get().specialty(), testOPStaff1.specialty());

      assertEquals(readOPStaff.get().role(), testOPStaff1.role());

      assertNotEquals(readOPStaff.get().lastUpdate(), testOPStaff1.lastUpdate());
    } else {
      throw new Exception();
    }
  }

  @Test
  public void testDeleteOPStaff() {

    repo.save(testOPStaff1);

    OPStaff.Delete deleteCommand = new OPStaff.Delete(
            testOPStaff1.id()
    );

    OPStaff deletedOPStaff = opStaffService.process(deleteCommand);

    assertFalse(repo.findOPStaff(deletedOPStaff.id()).isPresent());
  }

  @Test
  public void testGetOPStaff() throws Exception {
    repo.save(testOPStaff1);

    Optional<OPStaff> readOPStaff = opStaffService.getOPStaff(testOPStaff1.id());

    if (readOPStaff.isPresent()) {
      assertEquals(testOPStaff1.id(), readOPStaff.get().id());
      assertEquals(testOPStaff1.role(), readOPStaff.get().role());
      assertEquals(testOPStaff1.specialty(), readOPStaff.get().specialty());
    } else {
      throw new Exception();
    }
  }


  @Test
  public void testCreateOperationTeam() {

    repo.save(testOPStaff3);

    OperationTeam.Create createCommand = new OperationTeam.Create(
            "Team33",
            testOperationTeam2.opStaffs()
    );

    OperationTeam createdOperationTeam = operationTeamService.process(createCommand);

    assertTrue(
            repo.findOperationTeam(createdOperationTeam.id()).isPresent()
    );
    for(OPStaff opStaff : createdOperationTeam.opStaffs()) {
      assertTrue(repo.findOperationTeamOPStaff(createdOperationTeam.id(), opStaff.id()));
    }
  }


  @Test
  public void testDeleteOperationTeam() {

    repo.save(testOPStaff3);
    repo.save(testOperationTeam2);

    repo.assignOPStaffToOperationTeam(testOperationTeam2.id(), testOPStaff3.id());

    OperationTeam.Delete deleteCommand = new OperationTeam.Delete(
            testOperationTeam2.id()
    );

    OperationTeam deletedOperationTeam = operationTeamService.process(deleteCommand);

    assertFalse(repo.findOperationTeam(deletedOperationTeam.id()).isPresent());
  }

  @Test
  public void testGetOperationTeam() throws Exception {
    repo.save(testOPStaff3);
    repo.save(testOperationTeam2);

    repo.assignOPStaffToOperationTeam(testOperationTeam2.id(), testOPStaff3.id());

    Optional<OperationTeam> readOperationTeam = operationTeamService.getOperationTeam(testOperationTeam2.id());

    if (readOperationTeam.isPresent()) {
      assertEquals(testOperationTeam2.id(), readOperationTeam.get().id());
      assertEquals(testOperationTeam2.teamName(), readOperationTeam.get().teamName());

      for(int i = 0; i < readOperationTeam.get().opStaffs().size(); i++) {
        assertEquals(testOperationTeam2.opStaffs().get(i).id(), readOperationTeam.get().opStaffs().get(i).id());
      }
    } else {
      throw new Exception();
    }
  }

  @Test
  public void testAssignStaffToOperationTeam() {

    repo.save(testOperationTeam2);
    repo.save(testOPStaff3);

    OperationTeam.AssignStaff assignStaffCommand = new OperationTeam.AssignStaff(
            testOperationTeam2.id(),
            testOPStaff3.id()
    );

    OperationTeam assignedStaffOperationTeam = operationTeamService.process(assignStaffCommand);

    Boolean result = repo.findOperationTeamOPStaff(assignedStaffOperationTeam.id(), testOPStaff3.id());

    assertTrue(result);
  }

  @Test
  public void testRemoveOPStaffsInOperationTeams() {

    repo.save(testOperationTeam2);
    repo.save(testOPStaff3);

    repo.assignOPStaffToOperationTeam(testOperationTeam2.id(), testOPStaff3.id());

    OperationTeam.RemoveStaff removeStaffCommand = new OperationTeam.RemoveStaff(
            testOperationTeam2.id(),
            testOPStaff3.id()
    );

    OperationTeam removedTeam = operationTeamService.process(removeStaffCommand);

    assertEquals(testOperationTeam2.id().value(), removedTeam.id().value());
    assertFalse(repo.findOperationTeamOPStaff(testOperationTeam2.id(), testOPStaff3.id()));
  }


  @Test
  public void testCreatePreparationNote() {

    repo.save(testOperation);

    PreparationNote.Create createCommand = new PreparationNote.Create(
            ("Notiz erstellt mit Command"),
            testOperation.id()
    );

    PreparationNote createdPreparationNote = preparationNoteService.process(createCommand);

    assertTrue(
            repo.findPreparationNote(createdPreparationNote.id()).isPresent()
    );
  }

  @Test
  public void testUpdatePreparationNote() throws Exception {

    repo.save(testOperation);
    repo.save(testPreparationNote1);

    PreparationNote.Update updateCommand = new PreparationNote.Update(
            testPreparationNote1.id(),
            ("Notiz wurde geupdated")
    );

    PreparationNote updatePreparationNote = preparationNoteService.process(updateCommand);

    Optional<PreparationNote> readPreparationNote = repo.findPreparationNote(updatePreparationNote.id());

    if (readPreparationNote.isPresent()) {
      assertNotEquals(readPreparationNote.get().note(), testPreparationNote1.note());

      assertEquals(readPreparationNote.get().id(), testPreparationNote1.id());

      assertEquals(readPreparationNote.get().id(), testPreparationNote1.id());

      assertNotEquals(readPreparationNote.get().lastUpdate(), testPreparationNote1.lastUpdate());
    } else {
      throw new Exception();
    }
  }

  @Test
  public void testDeletePreparationNote() {

    repo.save(testOperation);
    repo.save(testPreparationNote1);

    PreparationNote.Delete deleteCommand = new PreparationNote.Delete(
            testPreparationNote1.id()
    );

    PreparationNote deletePreparationNote = preparationNoteService.process(deleteCommand);

    assertFalse(repo.findPreparationNote(deletePreparationNote.id()).isPresent());
  }

  @Test
  public void testGetPreparationNote() {

    repo.save(testOperation);
    repo.save(testPreparationNote2);

    Optional<PreparationNote> readPreparationNote = preparationNoteService.getPreparationNote(testPreparationNote2.id());

    if (readPreparationNote.isPresent()) {
      assertEquals(testPreparationNote2.id(), readPreparationNote.get().id());
      assertEquals(testPreparationNote2.note(), readPreparationNote.get().note());
      assertEquals(testPreparationNote2.operationId().value(), readPreparationNote.get().operationId().value());
    }
  }


  @Test
  public void testCreateRoom() throws Exception{

    Room.Create createCommand = new Room.Create(
            "Neuen Raum erstellt mit Command"
    );

    Room createdRoom = roomService.process(createCommand);

    assertTrue(
            repo.findRoom(createdRoom.id()).isPresent()
    );
  }

  @Test
  public void testUpdateRoom() throws Exception {

    repo.save(testRoom1);

    Room.Update updateCommand = new Room.Update(
            testRoom1.id(),
            ("NeuerRaumName")
    );

    Room updateRoom = roomService.process(updateCommand);

    Optional<Room> readRoom = repo.findRoom(updateRoom.id());

    if (readRoom.isPresent()) {
      assertNotEquals(readRoom.get().roomName(), testRoom1.roomName());

      assertEquals(readRoom.get().id(), testRoom1.id());

      assertNotEquals(readRoom.get().lastUpdate(), testRoom1.lastUpdate());
    } else {
      throw new Exception();
    }
  }

  @Test
  public void testDeleteRoom() throws Exception {

    repo.save(testRoom2);

    Room.Delete deleteCommand = new Room.Delete(
            testRoom2.id()
    );

    Room deleteRoom = roomService.process(deleteCommand);

    assertFalse(repo.findRoom(deleteRoom.id()).isPresent());
  }

  @Test
  public void testGetRoom() {
    repo.save(testRoom1);

    Optional<Room> readRoom = roomService.getRoom(testRoom1.id());

    if (readRoom.isPresent()) {
      assertEquals(testRoom1.id(), readRoom.get().id());
      assertEquals(testRoom1.roomName(), readRoom.get().roomName());
    }
  }
}


